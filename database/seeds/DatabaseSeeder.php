<?php

use App\Recipe;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run(): void
    {
        // You'd normally have this in a RecipeSeeder, but since we'll only have
        // one seeder for this project I'll inline the code here for simplicity.
        $fh = fopen(storage_path('recipe-data.csv'), 'r');
        if (!$fh) {
            throw new Exception("Couldn't read recipe_data.csv");
        }

        $i = 0;
        // This is the first row of the CSV, which we assume to be the header.
        $header = [];
        while (($row = fgetcsv($fh)) !== false) {
            // We'll save the first row into a variable so we can later get a nice associative
            // array of columnName => value instead of the index => value you'd get from fgetcsv.
            if ($i++ === 0) {
                $header = $row;
                continue;
            }

            $associativeRow = array_combine($header, $row);
            Recipe::updateOrCreate(['id' => $associativeRow['id']], $associativeRow);
        }
    }
}
