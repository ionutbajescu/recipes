<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');

            // I'm setting most of these as nullable for the purpose of the exercise.
            $table->string('box_type')->nullable();
            $table->string('title');
            $table->string('slug')->nullable();
            $table->string('short_title')->nullable();
            $table->string('marketing_description')->nullable();
            $table->unsignedInteger('calories_kcal')->nullable();
            $table->unsignedInteger('protein_grams')->nullable();
            $table->unsignedInteger('fat_grams')->nullable();
            $table->unsignedInteger('carbs_grams')->nullable();
            $table->string('bulletpoint1')->nullable();
            $table->string('bulletpoint2')->nullable();
            $table->string('bulletpoint3')->nullable();
            $table->string('season')->nullable();
            $table->string('base')->nullable();
            $table->string('protein_source')->nullable();
            $table->string('preparation_time_minutes')->nullable();
            $table->string('shelf_life_days')->nullable();
            $table->string('equipment_needed')->nullable();
            $table->string('origin_country')->nullable();
            $table->string('recipe_cuisine')->nullable();
            $table->string('in_your_box')->nullable();
            $table->integer('gousto_reference')->nullable();

            // This would normally reference another table, but
            // we don't have one in this exercise.
            $table->integer('recipe_diet_type_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
