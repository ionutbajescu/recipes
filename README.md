# Recipes

## How to use
This project uses Docker for the development environment. To run the service, execute:
```docker-compose up -d```

Then visit http://localhost:9000/recipes/1 in your browser / REST client. A demo of how the API docs would look like can also be found at http://localhost:9001.

To see the other endpoints the service can support, have a look at `routes/web.php`. Alternatively, you can look at the tests in `tests/RecipeTest.php`.

## Framework choice
After considering Laravel, Slim, Symfony and Lumen for this project I have decided to use Lumen for its speed and user(programmer)-friendliness.

## Persistent Storage
Given that one of the requirements was not use a database (although SQLite is acceptable), I have decided to go with SQLite to avoid rolling my own persistence layer for CSV.

There are two main reasons why you wouldn't use SQLite in production:
- SQLite is meant to be used by one user at a time, and therefore doesn't handle concurrency very well.
- Replication. There are some not-so-popular projects that mean to replicate SQLite (such as https://github.com/litereplica/litereplica) but at that point you'll be better off just using another database that supports replication natively.


## What kind of API consumers could use this service
This service assumes that the API consumer is trusted and that the service is within a locked down AWS VPC (so it cannot be accessed from the outside internet). To expose it to other consumers, additional security features must be implemented.

If the service was to be used by the backend of a trusted website, permissions would need to be added to it since the website would most probably not need to modify data. This only applies if the machine calling the service is trusted as opposed to calling this service directly from the frontend. If the frontend needs to call the service directly, read below for the security measures necessary for having the endpoint called by untrusted consumers.

Moreover, if the service was to be used in a mobile app, rate limiting and eventually authentication would need to be implemented on top of the permissions system. This also applies to REST calls by the website's frontend, which are by definition untrusted since the machine of the website's user does the HTTP call.

While the above security features could either be implemented in the traditional way, by writing the code directly into the service, that would only make the service more complicated and you'd need to write the logic all again for the next service.

If Services-oriented architecture / microservices are to be desired, the features can be implemented using the [AWS API Gateway](https://aws.amazon.com/api-gateway/), a separate microservice, or a mixture of both. Which one should be chosen depends on how the other services are architected and the decision can't be taken at a single-service level, but for the purpose of this exercise my personal preference would be the AWS API Gateway.

## State of tests
The code is "production quality" as required by the non-functional specs, but I have to admit that the tests only test the happy-path since I have decided that I could better spend the time explaining my thought process in this README file.

The code was developed using TDD (as in the tests have been written first), and I don't think any developer would have trouble maintaining this project but the testing suite could be a bit more comprehensive nonetheless.

Additionally, you'd never run tests against your production database and data.

## Deployment
Since the development of this service has been done in Docker, this service can be deployed using containers to ensure almost-complete parity between the staging and production environments. Additionally, an advantage of using Docker is that the service makes no assumptions of what's installed on the host machine, as long as Docker is installed, meaning that the service can scale very easily if hosted on services such as the Amazon Elastic Container Service.

I have already listed at least 2 AWS services that this service could use (ECS and API Gateway) besides the obvious ones that would have to be used anyway (application load balancers, route53, target groups etc) and someone obviously needs to create these services in AWS somehow. You would probably not want to do this manually if you're doing microservices, so the cloud infrastructure of this service could easily be orchestrated using [Terraform](https://www.terraform.io/).

## API Docs
Tests-first is great, and I couldn't live without it. But so is api-docs-at-least-last-but-not-never. Due to time constraints, there's no complete API documentation, but if this service was to be taken into production it'd be documented using the OpenAPI specification.

An advantage of documenting the REST APIs using a standard specification is that you can create API clients in different languages (PHP, Javascript or even Java for the mobile apps) automatically - and who doesn't love some automation!

An example (incomplete - contains only the `GET recipes/{id}` endpoint) for the swagger documentation can be found in the public/swagger.yml file.

## Interesting files
Just to make sure that the files that you really want to look at don't get lost in Lumen's plethora of by-default directory structure, you probably want to start your search from:
- tests/RecipeTest.php
- app/Http/Controllers/RecipeController.php
- app/Http/Controllers/RecipeRatingController.php
- database/seeds/DatabaseSeeder.php
- app/Recipe.php
- app/RecipeRating.php
- docker-compose.yml
- Makefile
