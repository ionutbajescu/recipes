<?php

namespace App\Http\Controllers;

use App\Recipe;
use App\RecipeRating;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RecipeRatingController extends Controller
{
    /**
     * @var RecipeRating
     */
    private $ratings;

    /**
     * @var Recipe
     */
    private $recipes;

    public function __construct(Recipe $recipes, RecipeRating $ratings)
    {
        $this->ratings = $ratings;
        $this->recipes = $recipes;
    }

    public function store(Request $request, int $id)
    {
        $this->validate($request, [
            'rating' => 'integer|min:1,max:5'
        ]);

        $created = $this->recipes->findOrFail($id)->ratings()->create($request->all());
        return response($created, Response::HTTP_CREATED);
    }
}
