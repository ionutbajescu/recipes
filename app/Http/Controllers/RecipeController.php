<?php

namespace App\Http\Controllers;

use App\Recipe;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RecipeController extends Controller
{
    /**
     * @var Recipe
     */
    private $recipes;

    public function __construct(Recipe $recipes)
    {
        $this->recipes = $recipes;
    }

    public function show(int $id): Recipe
    {
        return $this->recipes->findOrFail($id);
    }

    public function update(Request $request, int $id): array
    {
        $recipe = $this->recipes->findOrFail($id);
        return ['updated' => $recipe->update($request->all())];
    }

    public function store(Request $request): Response
    {
        $created = $this->recipes->create($request->all());
        return response($created, Response::HTTP_CREATED);
    }

    public function byCuisine(string $cuisine): LengthAwarePaginator
    {
        return $this->recipes->whereRecipeCuisine($cuisine)->paginate();
    }
}
