test:
	docker-compose run --rm php ./vendor/bin/phpunit

db:
	docker-compose run --rm php sh -c './artisan migrate -v && ./artisan db:seed -v'
