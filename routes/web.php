<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->get('/', function () {
    return "It's working!";
});

$router->get('/cuisines/{cuisine}/recipes', 'RecipeController@byCuisine');

$router->get('/recipes/{recipe}', 'RecipeController@show');

$router->post('/recipes', 'RecipeController@store');

$router->put('/recipes/{recipe}', 'RecipeController@update');

$router->post('/recipes/{recipe}/ratings', 'RecipeRatingController@store');
