<?php

use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseMigrations;

/**
 * I'm using snake_case in the test methods so they're more readable.
 * Writing them in camelCase is more common though, and definitely
 * preferred if that's what the rest of the team is using.
 */
class RecipeTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->artisan('db:seed');
    }
    
    /**
     * @test
     */
    public function it_can_return_the_first_recipe()
    {
        $this->get('recipes/1');

        $this->seeStatusCode(Response::HTTP_OK);
        $this->seeJsonEquals([
            'id' => 1,
            'box_type' => 'vegetarian',
            'title' => 'Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad',
            'slug' => 'sweet-chilli-and-lime-beef-on-a-crunchy-fresh-noodle-salad',
            'short_title' => '',
            'marketing_description' => 'Here we\'ve used onglet steak which is an extra flavoursome cut of beef that should never be cooked past medium rare. So if you\'re a fan of well done steak, this one may not be for you. However, if you love rare steak and fancy trying a new cut, please be',
            'calories_kcal' => '401',
            'protein_grams' => '12',
            'fat_grams' => '35',
            'carbs_grams' => '0',
            'bulletpoint1' => '',
            'bulletpoint2' => '',
            'bulletpoint3' => '',
            'recipe_diet_type_id' => 'meat',
            'season' => 'all',
            'base' => 'noodles',
            'protein_source' => 'beef',
            'preparation_time_minutes' => '35',
            'shelf_life_days' => '4',
            'equipment_needed' => 'Appetite',
            'origin_country' => 'Great Britain',
            'recipe_cuisine' => 'asian',
            'in_your_box' => '',
            'gousto_reference' => '59',
            'created_at' => '30/10/2019 17:10:00',
            'updated_at' => '30/10/2019 17:10:00',
        ]);
    }

    /**
     * @test
     */
    public function it_returns_404_if_page_does_not_exist()
    {
        $this->get('recipes/0');

        $this->seeStatusCode(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function it_filters_recipes_by_cuisine()
    {
        $this->get('cuisines/mexican/recipes');

        $this->seeStatusCode(Response::HTTP_OK);
        $response = json_decode($this->response->content(), true);
        $this->assertCount(1, $response['data']);
        $this->assertArraySubset(['recipe_cuisine' => 'mexican'], $response['data'][0]);
    }

    /**
     * @test
     */
    public function it_updates_a_recipe()
    {
        $title = 'The first title';

        // Make sure the recipe doesn't already have the title we want to test with.
        $this->missingFromDatabase('recipes', [
            'id' => 2,
            'title' => $title
        ]);

        // Update the recipe and then check if it now has the right title.
        $this->put('recipes/2', compact('title'));
        $this->seeStatusCode(Response::HTTP_OK);
        $this->seeInDatabase('recipes', [
            'id' => 2,
            'title' => $title
        ]);
    }

    /**
     * @test
     */
    public function it_can_store_a_new_recipe()
    {
        $title = "I'm a new recipe!";

        // Check that the database is in the right state
        $this->missingFromDatabase('recipes', [
            'title' => $title
        ]);
        $this->assertEquals(10, $this->app->db->table('recipes')->count());

        // Add the title and check that it has indeed been added
        $this->post('recipes', compact('title'));
        $this->seeStatusCode(Response::HTTP_CREATED);
        $this->assertEquals(11, $this->app->db->table('recipes')->count());
        $this->seeInDatabase('recipes', compact('title'));
    }

    /**
     * @test
     */
    public function it_stores_the_rating_of_a_recipe()
    {
        $this->assertEquals(0, $this->app->db->table('recipe_ratings')->count());

        $this->post('recipes/1/ratings', ['rating' => 5]);

        $this->seeStatusCode(Response::HTTP_CREATED);
        $this->seeInDatabase('recipe_ratings', [
            'rating' => 5,
            'recipe_id' => 1
        ]);
    }
}
